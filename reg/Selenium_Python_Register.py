from selenium import webdriver
from webdriver_manager.opera import OperaDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from data import *
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import unittest


class TestRegister(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Opera(executable_path=OperaDriverManager().install())
        self.driver.maximize_window()
        self.driver.get(MainSite)
        self.timeout = 5
        try:
            element_present = ec.presence_of_element_located((By.XPATH, RegistrationPath))
            WebDriverWait(self.driver, self.timeout).until(element_present)

        except NoSuchElementException:
            print("Page load timeout")

    def test_registration(self):
        self.driver.find_element_by_xpath(RegistrationPath).click()

        try:
            element_present = ec.presence_of_element_located((By.XPATH, FullNameFieldPath))
            WebDriverWait(self.driver, self.timeout).until(element_present)

        except NoSuchElementException:
            print("Page load timeout")

        self.driver.find_element_by_xpath(FullNameFieldPath).send_keys(NameKey)
        self.driver.find_element_by_xpath(EmailFieldPath).send_keys(EmailKey)
        self.driver.find_element_by_xpath(PasswordFieldPath).send_keys(PasswordKey)
        self.driver.find_element_by_xpath(CompanyFieldPath).send_keys(CompanyKey)
        self.driver.find_element_by_xpath(PhoneFieldPath).send_keys(PhoneKey)
        self.driver.find_element_by_xpath(AgreeLicencePath).click()
        self.driver.find_element_by_xpath(SubmitPath).submit()

        #logout
        try:
            element_present = ec.presence_of_element_located((By.XPATH, DropdownLoginPath))
            WebDriverWait(self.driver, self.timeout).until(element_present)

        except NoSuchElementException:
            print("Page load timeout")
        self.driver.find_element_by_xpath(DropdownLoginPath).click()

        try:
            element_present = ec.presence_of_element_located((By.XPATH, LogoutPath))
            WebDriverWait(self.driver, self.timeout).until(element_present)

        except NoSuchElementException:
            print("Page load timeout")

        self.driver.find_element_by_xpath(LogoutPath).click()

    def test_LoginCredential(self):
        self.driver.find_element_by_xpath(LoginPath).click()

        # login page
        try:
            element_present = ec.presence_of_element_located((By.XPATH, LoginEmailFieldPath))
            WebDriverWait(self.driver, self.timeout).until(element_present)

        except NoSuchElementException:
            print("Page load timeout")

        self.driver.find_element_by_xpath(LoginEmailFieldPath).send_keys(EmailKey)
        self.driver.find_element_by_xpath(LoginPasswordFieldPath).send_keys(PasswordKey)
        self.driver.find_element_by_xpath(LoginConfirmButton).click()

        # error compare
        fail_message = self.driver.find_element_by_xpath(ErrorXpathMessage).text
        assert (fail_message == ExpectedErrorMessage)

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
