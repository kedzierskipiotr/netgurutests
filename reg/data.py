from faker import Faker
fake = Faker()
from data import *



#website
MainSite = "https://www.lambdatest.com/"

#locators on mainpage
RegistrationPath = '//*[@id="navbarSupportedContent"]/ul/li[7]/a'
LoginPath = '//*[@id="navbarSupportedContent"]/ul/li[6]/a'


#locators on register page
FullNameFieldPath = '//*[@id="app"]/div/div/div[2]/div/form/div[1]/input'
EmailFieldPath = '//*[@id="app"]/div/div/div[2]/div/form/div[2]/input'
PasswordFieldPath = '//*[@id="userpassword"]'
CompanyFieldPath = '//*[@id="app"]/div/div/div[2]/div/form/div[4]/input'
PhoneFieldPath = '//*[@id="app"]/div/div/div[2]/div/form/div[5]/input'
AgreeLicencePath = '//*[@id="app"]/div/div/div[2]/div/form/div[6]/label/samp'
SubmitPath = '//*[@id="app"]/div/div/div[2]/div/form/div[7]/button'

#locator after login
DropdownLoginPath = '//*[@id="navbarDropdown"]'
LogoutPath = '/html/body/div[1]/nav/div/div/ul[2]/li/div/a[4]'

#locators login page
LoginEmailFieldPath = '//*[@id="app"]/div/div/div/div/form/div[1]/input'
LoginPasswordFieldPath = '//*[@id="userpassword"]'
LoginConfirmButton = '//*[@id="app"]/div/div/div/div/form/div[3]/button'
ErrorXpathMessage = '//*[@id="app"]/div/div/div/div/form/div[1]/p'

#keys
NameKey = fake.name()
EmailKey = fake.ascii_email()
PasswordKey = "NetGuru123"
CompanyKey = fake.company()
PhoneKey = fake.phone_number()

#Error message
ExpectedErrorMessage = 'Please enter a correct username and password. Note that the password is case-sensitive'









